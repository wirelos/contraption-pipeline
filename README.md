# Contraption Pipeline

## Setup pipeline
Login to concourse server.
```sh
SERVER=citadel.lan:8080
fly -t local login -c http://$SERVER
```

add ssh private keys:
```sh
echo "gitlab-key: |" > ~/keystore/concourse-vars.yaml
cat ~/.ssh/id_rsa >> ~/keystore/concourse-vars.yaml
```
Add contraption pipeline to concourse
```sh
fly -t local set-pipeline -p contraption -c pipeline.yml -l ~/keystore/concourse-vars.yaml
```

Activate the pipeline
```sh
fly -t local up -p contraption
```

## Update pipeline
```sh
fly -t local set-pipeline -p contraption -c pipeline.yml -l ~/keystore/concourse-vars.yaml
```
